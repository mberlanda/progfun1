
object rationals {

  // A rational number is represented by x/y
  // def addRationalNumerator(n1: Int, d1: Int, n2: Int, d2: Int) : Int
  // def addRationalDenominator(n1: Int, d1: Int, n2: Int, d2: Int) : Int

  class Rational(x: Int, y: Int) {
    require(y != 0, "denominator must be nonzero")

    def this(x: Int) = this(x, 1)

    private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    private val g = gcd(x, y)
    def numer = x / g
    def denom = y / g

    // def less(that: Rational) =
    def < (that: Rational) =
      this.numer * that.denom < that.numer * this.denom

    def max(that: Rational) =
      if (this < that) that else this

    // def add(that: Rational) =
    def +(that: Rational) =
      new Rational(
        numer * that.denom + that.numer * denom,
        denom * that.denom
      )

    // def sub(that: Rational) =
    //   new Rational(
    //     numer * that.denom - that.numer * denom,
    //     denom * that.denom
    //   )


    // def neg: Rational = new Rational(-numer, denom)
    def unary_- : Rational = new Rational(-numer, denom)
    def - (that: Rational) = this + -that

    // def mul(that: Rational) =
    def *(that: Rational) =
      new Rational(
        numer * that.numer,
        denom * that.denom
      )

    // def div(that: Rational) =
    def /(that: Rational) =
      new Rational(
        numer * that.denom,
        that.numer * denom
      )

    // def equal(that: Rational) =
    def ==(that: Rational): Boolean =
      numer * that.denom == that.numer * denom

    override def toString: String = numer + "/" + denom
  }

  val x = new Rational(1, 3)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)

  x + y * z
  x - y - z
  y + y
  x max z

  assert((x max z) == z)
  // assert(x.max(z).equal(x)) // AssertionError
  // val strange = new Rational(1, 0) // IllegalArgumentException

  new Rational(3)

  // infix notation
  // relaxed identifiers
}
