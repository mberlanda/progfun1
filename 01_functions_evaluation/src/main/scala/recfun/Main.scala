package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if (c < 0) 0 else
      c match {
        case 0   => 1
        case `r` => 1
        case _   =>  pascal(c-1, r-1) + pascal(c, r-1)
      }
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def _checkBalance(chars: List[Char], score: Int): Boolean =
        if(chars.isEmpty) score == 0
        else {
          val new_score = score + _balanceScore(chars.head)
          if (new_score < 0) false else _checkBalance(chars.tail, new_score)
        }

      def _balanceScore(char: Char) : Int = {
        char match {
          case '(' =>  1
          case ')' => -1
          case  _  =>  0
        }
      }
      _checkBalance(chars, 0)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      money compare 0 match {
        case 1   =>
          if (coins.isEmpty) 0
          else countChange(money, coins.tail) + countChange(money - coins.head, coins)
        case 0   => 1
        case -1  => 0
      }
    }
  }
