# Functional Programming Principles in Scala

This repo contains some materials and assignments from Martin Odersky 's [Functional Programming Principles in Scala](https://www.coursera.org/learn/progfun1/)

# Week 1

### Getting Started

- Obtain the [Project Files](alaska.epfl.ch/~dockermoocs/progfun1/example.zip)
- Use the Scala REPL

```
$ cd 01_getting_started/
$ sbt
> console
scala> println("Oh, hai!")
Oh, hai!

scala> val l = List(1, 2, 3)
l: List[Int] = List(1, 2, 3)

scala> val squares = l.map(x => x * x)
squares: List[Int] = List(1, 4, 9)
```

- Open the Project in Intellij
- Run the Code
- Write Tests
- Submit Solution

```
> submit your.email@domain.com <TOKEN>
[info] Compiling 1 Scala source to ~/progfun1/01_getting_started/target/scala-2.11/test-classes...
[info] Packaging ~/progfun1/01_getting_started/target/scala-2.11/progfun1-example_2.11-0.1-SNAPSHOT.jar ...
[info] Done packaging.
[info] Attempting to submit "example" assignment in "progfun1" course
[info] Using:
[info] - email: your.email@domain.com
[info] - submit token: <TOKEN>
[info] Connecting to Coursera...
[info] Successfully connected to Coursera. (Status 201)
[info]
[info] Assignment submitted successfully!
[info]
[info] You can see how you scored by going to:
[info] https://www.coursera.org/learn/progfun1/programming/xIz9O/
[info]
[info] and clicking on "My Submission".
[success] Total time: 6 s, completed 06-Feb-2018 09:10:10
```

### Functions & Evaluation

```scala
def abs(x: Double) = if (x < 0) -x else x

def sqrt(x: Double) : Double = {

  def sqrtIter(guess: Double): Double =
    if (isGoodEnough(guess)) guess
    else sqrtIter(improve(guess))

  def isGoodEnough(guess: Double) =
    abs(guess * guess - x ) < 0.001  * x

  def improve(guess: Double) =
    (guess + x / guess) / 2

  sqrtIter(1.0)
}
```

# Week 2

### Higher Order Functions

```scala

def sumInts(a: Int, b: Int): Int =
	if (a > b) 0 else a + sumInts(a+1, b)

def cube(x: Int): Int = x * x * x

def sumCubes(a: Int, b: Int): Int =
	if (a > b) 0 else cube(a) + sumCubes(a+1, b)

def sum(f: Int => Int, a: Int, b: Int)
	if (a > b) 0 else a + sum(f, a+1, b)

def sumInts(a: Int, b: Int): Int =
	sum(id, a, b)

def id(x: Int): Int = Int

def sumInts(a: Int, b: Int): Int =
	sum(x => x, a, b)

// Linear tail recursion

def sum(f: Int => Int)(a: Int, b: Int): Int =
	def loop(a: Int, acc: Int): Int = {
		if (a > b) acc
		else loop(a + 1, acc + f(a))
	}
	loop(a, 0)

// Currying

def sum(f: Int => Int): (Int, Int) => Int = {
	def sumF(a: Int, b: Int): Int = {
		def loop(a: Int, acc: Int): Int = {
			if (a > b) acc
			else loop(a + 1, acc + f(a))
		}
		loop(a, 0)
	}
	sumF
}

def sumInts = sum(x => x)
sum (cube) (1, 10)

// product
def product(f: Int => Int) (a: Int, b: Int): Int = {
	if (a > b) 1
	else f(a) * product (f) (a + 1, b)
}
def fact(n : Int) = product (x => x)(1, n)

def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int) (a: Int, b: Int): Int = {
	if (a > b) zero
	else combine(f(a), mapReduce (f, combine, zero) (a + 1, b))
}

def product(f: Int => Int) = mapReduce(f, (x, y) => x * y, 1)
```

Playin around with the assignment:

```scala

import funsets.FunSets

val s1 = FunSets.singletonSet(1)
FunSets.toString(FunSets.map(s1, (x => x + 5)))

val s2 = FunSets.singletonSet(2)
val s3 = FunSets.union(s1, s2)

FunSets.toString(s3)
FunSets.exists(s1, s2)
FunSets.exists(s3, s2)

def even (x: Int): Boolean = (x % 2) == 0
FunSets.toString(FunSets.filter(s3, even))
```

# Week 3

### Data and Abstraction

[Lecture 3.1 intsets lecture worksheet](03_data_and_abstraction/src/main/scala/ClassHierarchies.sc)

[Lecture 3.1 Hello program](03_data_and_abstraction/src/main/scala/ClassHierarchies.sc)
```
sbt "runMain Hello"
sbt "runMain objsets.Main"
```

