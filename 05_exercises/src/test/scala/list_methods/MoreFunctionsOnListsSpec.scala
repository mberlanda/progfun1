package list_methods

import org.scalatest.FlatSpec
import list_methods.MoreFunctionsOnLists._


class MoreFunctionsOnListsSpec extends FlatSpec {

  "last" should "raise 'last of empty list' Error" in {
    val emptyList = List()
    assertThrows[Error]{ last(emptyList) }
  }

  it should "work for 1 item list" in {
    val oneItemList = List(1)
    assert(last(oneItemList) == 1)
  }

  it should "work for n items list" in {
    val nItemList = List(1, 2, 3)
    assert(last(nItemList) == 3)
  }

  "init" should "raise 'init of empty list' Error" in {
    val emptyList = List()
    assertThrows[Error]{ init(emptyList) }
  }

  it should "work for 1 item list" in {
    val oneItemList = List(1)
    assert(init(oneItemList) == List())
  }

  it should "work for n items list" in {
    val nItemList = List(1, 2, 3)
    assert(init(nItemList) == List(1, 2))
  }

  "concat" should "work for two empty lists" in {
    assert(concat(List(), List()) == List())
  }
  it should "work for one empty list" in {
    assert(concat(List(1, 2), List()) == List(1, 2))
  }
  it should "work for two non empty lists" in {
    assert(concat(List(1, 2), List(3, 4)) == List(1, 2, 3, 4))
  }

  "reverse" should "work for an empty list" in {
    assert(reverse(List()) == List())
  }

  it should "work for 1 item list" in {
    assert(reverse(List(1)) == List(1))
  }

  it should "work for n items list" in {
    assert(reverse(List(1, 2, 3)) == List(3, 2, 1))
  }

  "removeAr" should "work as expected" in {
    assert(removeAt(1, List('a', 'b', 'c', 'd')) == List('a', 'c', 'd'))
  }
}
